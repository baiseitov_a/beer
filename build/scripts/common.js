/* прелоадер */
$(window).on('load', function () {
    var $preloader = $('#js-preloader'),
        $spinner   = $preloader.find('.preloader__spinner');
    $spinner.fadeOut();
    $preloader.delay(450).fadeOut('slow');
});

$(document).ready(function(){

/* Слайдер на главной стр */
  $('#js-slider').owlCarousel({
  	items: 1,
    smartSpeed: 500,
  	loop:true,
    nav:true,
    dots: true,
    navText: ["<img src='/images/icons/slide-prev.png'>","<img src='/images/icons/slide-next.png'>"]
  });

/* Слайдер на стр адреса магазинов */

var galleryTop = new Swiper('.gallery-top', {        
        spaceBetween: 10
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 20,
        centeredSlides: true,
        slidesPerView: 3,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;
    
/* accordion меню */

$( ".js-accordion" ).accordion({
      collapsible: true,
      active: false
    });

/* popup */
  $('.popup-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom'
  });


/* Api Яндекс карта */

  ymaps.ready(init);
  ymaps.ready(initSecond);
 
    function init(){     
     
        var myMap;
     
        myMap = new ymaps.Map("js-map-1", {
            center: [47.271975074248026,39.69305799999998],
            zoom: 16,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');
 
        myMap.controls.add("zoomControl", {
            position: {top: 15, left: 15}
        });

        myPlacemark0 = new ymaps.Placemark([47.271975074248026,39.69305799999998], {}, {
            iconLayout: 'default#image',
            iconImageHref: '/images/icons/placemark.png', // картинка иконки
            iconImageSize: [40, 59], // размер иконки
            iconImageOffset: [-40, -59] // позиция иконки
            
        });
        /* Добавляем метки на карту */
        myMap.geoObjects
            .add(myPlacemark0);
     
    };

    function initSecond(){     
     
        var myMap;
     
        myMap = new ymaps.Map("js-map-2", {
            center: [47.271975074248026,39.69305799999998],
            zoom: 16,
            controls: []
        });

        myMap.behaviors.disable('scrollZoom');
 
        myMap.controls.add("zoomControl", {
            position: {top: 15, left: 15}
        });

        myPlacemark0 = new ymaps.Placemark([47.271975074248026,39.69305799999998], {}, {
            iconLayout: 'default#image',
            iconImageHref: '/images/icons/placemark.png', // картинка иконки
            iconImageSize: [40, 59], // размер иконки
            iconImageOffset: [-40, -59] // позиция иконки
            
        });
        /* Добавляем метки на карту */
        myMap.geoObjects
            .add(myPlacemark0);
     
    };
    

});